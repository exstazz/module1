using System;

namespace M1
{
    public class Module1
    {
        static void Main(string[] args)
        {
            Module1 myClass = new Module1();
            myClass.SwapItems(3, 5);
            int[] dd = new int[3] { 5,3,18};
            myClass.GetMinimumValue(dd);
        }
        public int[] SwapItems(int a, int b)
        {
            int c;
            int[] result = new int[2];
            c = a;
            a = b;
            b = c;
            result[0] = a;
            result[1] = b;
            Console.WriteLine(a);
            Console.WriteLine(b);
            return result;
        }
        public int GetMinimumValue(int[] input)
        {
            int minValue;
            minValue = input[0];
            for (int i = 1; i < input.Length; i++)
            {
                if (input[i] < minValue)
                {
                    minValue = input[i];
                }
            }
            Console.WriteLine(minValue);
            return minValue;
        }
    }
}
